import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChange,
  SimpleChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy,
  ViewChild,
  ElementRef,
  ContentChild
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements
  OnInit,
  OnChanges,
  DoCheck,
  OnDestroy,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked {

  @Input('srvElement') element: { type: string, name: string, content: string };
  @Input() name: string;
  @ViewChild('heading') header: ElementRef;
  @ContentChild('contentParagraph') paragraph: ElementRef;

  constructor() {
    console.log("construtor chamado");
  }

  ngOnInit() {
    console.log("ngOnInit method call");
    console.log("Text Content : " + this.header.nativeElement.textContent);
    console.log("Text Content of paragraph : " + this.paragraph.nativeElement.textContent);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("ngOnChanges method call");
    console.log(changes);
  }

  ngDoCheck() {
    console.log("ngDoCheck method call");
  }

  ngAfterContentInit() {
    console.log("ngAfterContentInit method call");
  }

  ngAfterContentChecked() {
    console.log("ngAfterContentChecked method call");
  }

  ngAfterViewInit() {
    console.log("ngAfterViewInit method call");
    console.log("Text Content : " + this.header.nativeElement.textContent);
    console.log("Text Content of paragraph : " + this.paragraph.nativeElement.textContent);

  }

  ngAfterViewChecked() {
    console.log("ngAfterViewChecked method call");

  }

  ngOnDestroy() {
    console.log("ngOnDestroy method call");
  }
}
